"""
Thing  Mapper
jhenry6@gmail.com

Handy Dandy mapper script to build an interactive map with a particular input of cities to mark locations.
Because doing things manually is just......stupid.
This is nice for the purpose if you have multiple sites you want to plot on a map. 
"""
from tinydb import TinyDB, where, Query
from geopy.geocoders import Nominatim
import folium
geolocator = Nominatim()

def main():
    def geo_lookup(city_name):
        geo_query = Query()
        if not db.search((geo_query.city == city_name)): 
            location = geolocator.geocode(city_name)
            db.insert({'city':city_name, 'latitude':location.latitude, 'longitude':location.longitude})
            return list([city_name, location.latitude, location.longitude])
        else:
            q = db.search((geo_query.city == city_name))[0]
            return (q['city'],q['latitude'],q['longitude'])

        
    def build_list(city_list):
        master_list = [geo_lookup(x) for x in city_list]
        return master_list
    def build_map(thing_list):
        map = folium.Map(location=[32.3512601, -95.3010623], zoom_start=5)
        for x,y,z in thing_list:
            map.polygon_marker(location=[y,z], popup=x)
        map.create_map(path='thing_map.html')

    thing_list = build_list(your_city_list)
    build_map(thing_list)
if __name__ == '__main__':
    # You are more than welcome to read cities in based on CSV etc. 
    your_city_list = ["Lindale, TX",
                      "Dallas, TX",
                      "Seattle, WA"]
    db = TinyDB('./geo.json')
    main()


